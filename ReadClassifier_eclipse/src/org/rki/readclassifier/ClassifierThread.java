package org.rki.readclassifier;


public class ClassifierThread extends Thread {

	private Reader reader;
	private Writer writer;
	private ReadClassifier readClassifier;
	
	public ClassifierThread(Reader reader, Writer writer, ReadClassifier readClassifier) {
		super();
		this.reader = reader;
		this.writer = writer;
		this.readClassifier = readClassifier;
	}
	
	public void run() {
		try {
			while(true) {
				ReadPair reads = reader.reads.take();
                                if(reads.read1 == null) {
                                    writer.outReads.put(new ReadPair(null, null));
                                    return;
                                }
                                reads.read1.bases = reads.read1.bases.toUpperCase();
                                if(reads.read2 != null)
                                	reads.read2.bases = reads.read2.bases.toUpperCase();
				readClassifier.getScore(reads);
                                reads.read1.toString();
                                if(reads.read2 != null)
                                	reads.read2.toString();
                                writer.outReads.put(reads);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}