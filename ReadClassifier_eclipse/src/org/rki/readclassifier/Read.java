package org.rki.readclassifier;

public class Read {
	public boolean invalid = false;
	public String name;
	public String bases;
	public String qualities;
	public double score = 0;
	public double score1 = 0;
	public double score2 = 0;
        public String fastq = null;
        public String fasta = null;
	
	public Read(String name, String bases, String qualities) {
		this.name = name;
		this.bases = bases;
		this.qualities = qualities;
		this.score1 = 0;
		this.score2 = 0;
	}
	
	public String toString(boolean showQuals) {
		if(qualities != null) {
			return toFastq(showQuals);
                }
		else
			return toFasta(showQuals);
	}
     
	public void normalizeTo(int length) {
		if(length == -1)
			return;
		float fact = (float)length/(float)bases.length(); 
		score *= fact;
		score1 *= fact;
		score2 *= fact;
	}
	
	public String toFasta(boolean showQuals) {
        if(fasta == null) {
			StringBuilder res = new StringBuilder();
			res.append(name);
			if(showQuals) {
				res.append("_");
				res.append(score1);
				res.append("_");
				res.append(score2);
            }
			res.append("\n");
			res.append(bases);
			res.append("\n");
            fasta = res.toString();
        }
        return fasta;
	}
	
	public String toFastq(boolean showQuals) {
        if(fastq == null) {
			StringBuilder res = new StringBuilder();
			res.append(name);
			if(showQuals) {
				res.append("_");
				res.append(score1);
				res.append("_");
				res.append(score2);
			}
			res.append("\n");
			res.append(bases);
			res.append("\n+\n");
			res.append(qualities);
			res.append("\n");
			fastq = res.toString();
        }
        return fastq;
	}
}
